# -*- coding: utf-8 -*-
# Generated by Django 1.11.18 on 2019-03-15 20:38
from __future__ import unicode_literals

from django.db import migrations


USERNAME = 'hinj'
NEW_CERT = '/DC=org/DC=cilogon/C=US/O=LIGO/OU=Robots/CN=ldas-grid.ligo.caltech.edu/CN=hwinjmonitor/CN=Thomas Massinger/CN=UID:thomas.massinger.robot'
OLD_EMAIL = 'mhulko@caltech.edu'
NEW_EMAIL = 'tmassing@ligo.caltech.edu'


def update_acct(apps, schema_editor):
    RobotUser = apps.get_model('ligoauth', 'RobotUser')

    # Get user
    user = RobotUser.objects.get(username=USERNAME)

    # Create new certificate for user
    user.x509cert_set.create(subject=NEW_CERT)

    # Update email
    user.email = NEW_EMAIL
    user.save(update_fields=['email'])


def revert_acct(apps, schema_editor):
    RobotUser = apps.get_model('ligoauth', 'RobotUser')

    # Get user
    user = RobotUser.objects.get(username=USERNAME)

    # Delete new certificate
    user.x509cert_set.get(subject=NEW_CERT).delete()

    # Revert email
    user.email = OLD_EMAIL
    user.save(update_fields=['email'])


class Migration(migrations.Migration):

    dependencies = [
        ('ligoauth', '0025_update_emfollow_certs'),
    ]

    operations = [
        migrations.RunPython(update_acct, revert_acct),
    ]
