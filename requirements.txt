ConcurrentLogHandler==0.9.1
Django==1.11.23
django-debug-toolbar==1.9.1
django-extensions==2.0.5
django-guardian==1.4.9
django-model-utils==3.1.1
django-ses==0.8.10
django-silk==3.0.1
django-twilio==0.9.0
django-user-sessions==1.6.0
djangorestframework==3.9.0
djangorestframework-guardian==0.1.1
dnspython==1.15.0
flake8==3.5.0
gunicorn[gthread]==19.9.0
html5lib==1.0.1
ipdb==0.10.2
ipython==5.5.0
lalsuite==6.53 # undocumented requirement for lscsoft-glue
ligo-lvalert==1.5.6
# Temporary pin of lvalert-overseer for Python 3 update
git+https://git.ligo.org/lscsoft/lvalert-overseer.git@python3-overseer#egg=ligo-lvalert-overseer
#ligo-lvalert-overseer==0.1.3
lscsoft-glue==1.60.0
lxml==4.2.0
matplotlib==2.0.0
mock==2.0.0
mysqlclient==1.4.2
numpy==1.17.2
packaging==17.1
phonenumbers==8.8.11
python-ldap==3.1.0
python-memcached==1.59
scipy==1.2.1
sentry-sdk==0.7.10
service_identity==17.0.0
simplejson==3.15.0
Sphinx==1.7.0
twilio==6.10.3
voevent-parse==1.0.3
# Do NOT upgrade pyparsing beyond 2.3.0 (even to 2.3.1) without carefully
# testing and modifying the search query code. There were a number of
# problematic API changes in 2.3.1 and beyond that will need to be handled
# appropriately.
pyparsing==2.3.0
pytest<5; python_version < '3'
pytest==5.1.2; python_version >= '3'
pytest-cov==2.6.1
pytest-django==3.4.8
pytz==2018.9
# Fixed at old versions to prevent bugs:
# https://github.com/fritzy/SleekXMPP/issues/477
# https://github.com/etingof/pyasn1/issues/112
pyasn1==0.3.6
pyasn1-modules==0.1.5
# Installing futures for gunicorn gthreads (Python 2 only):
futures==3.2.0; python_version < '3'
